import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    constructor(private router: Router) { }
    loginForm: FormGroup;

    ngOnInit() { 
        this.loginForm = new FormGroup({
            email: new FormControl('',[Validators.required,Validators.email]),
            password: new FormControl('',[Validators.required]),
          });
    }

    onLogin() {
        console.log(this.loginForm)
        localStorage.setItem('isLoggedin', 'true');
        localStorage.setItem('userType', 'admin');
        this.router.navigate(['/dashboard']);
    }
}
