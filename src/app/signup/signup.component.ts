import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignUpComponent implements OnInit {
    constructor(private router: Router) { }
    signUpForm: FormGroup;

    ngOnInit() { 
        this.signUpForm = new FormGroup({
            name:new FormControl("",[Validators.required]),
            email: new FormControl('',[Validators.required, Validators.email]),
            password: new FormControl('',[Validators.required]),
            yearofpass:new FormControl('',[Validators.required]),
            phonenumber: new FormControl('', [Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]),
            type:new FormControl('',[Validators.required])
          });
    }

    onSignUp() {
        console.log(this.signUpForm)
        localStorage.setItem('isLoggedin', 'true');
        localStorage.setItem('userType', 'admin');
        //this.router.navigate(['/dashboard']);
    }
}
