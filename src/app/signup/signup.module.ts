import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '../shared/modules/material/material.module';
import { SignUpComponent } from './signup.component';
import { SignUpRoutingModule } from './signup-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        SignUpRoutingModule,
        MaterialModule,
        ReactiveFormsModule,
        FlexLayoutModule.withConfig({ addFlexToParent: false })
    ],
    declarations: [SignUpComponent]
})
export class SignUpModule { }
