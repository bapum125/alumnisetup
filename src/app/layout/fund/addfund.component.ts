import { Component, OnInit,ViewChild ,ElementRef} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-addfund',
  templateUrl: './addfund.component.html',
  styleUrls: ['./fund.component.scss']
})
export class AddFundComponent implements OnInit {
  paymentType=[{name:'Debit'},{name:'Credit'}]
  buttonName: string;
  formName: string;
  fundId:any;
  updateJson:any;
  expenseForm: FormGroup;
  constructor(private router: ActivatedRoute) { }
 
  ngOnInit() {
    this.buttonName = "Add event"
    this.formName = "Add"
    this.router.params.subscribe(params => {
      this.fundId = params['id']; 
      console.log(this.fundId)
      if(this.fundId !=undefined )
      {
        this.buttonName = "Update event"
        this.formName = "Update"
        this.updateJson = {
          fundTitle:"",
          exprenseDescription:"",
          date:null,
          creditdebit:"",
          amount:"",
        }
      }else{
        this.updateJson = {
          fundTitle:"",
          exprenseDescription:"",
          date:null,
          creditdebit:"",
          amount:"",
        }
      }
   });
    this.expenseForm = new FormGroup({
      amount:new FormControl("",[Validators.required]),
      type: new FormControl('',[Validators.required]),
      expensedate: new FormControl('',[Validators.required]),
      description:new FormControl('',[Validators.required]),
      expenseincome: new FormControl('',[Validators.required]),
    });
}
onExpensePost(){

}

}
