import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FundComponent } from './fund.component';
import { AddFundComponent } from './addfund.component';
import { ViewFundComponent } from './viewfund.component';

const routes: Routes = [
    {
        path: '', component: FundComponent,
    },
    { path: 'add', component: AddFundComponent },
    { path: 'edit/:id', component: AddFundComponent },
    { path: 'view/:id', component: ViewFundComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FundRoutingModule {}
 