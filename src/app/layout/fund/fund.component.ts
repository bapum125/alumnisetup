import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';

export interface UserData {
  id:number;
  expanseincomename: string;
  creditordebit: string;
  date: string;
  description: string;
  position:number;
  amount:number
}
const ELEMENT_DATA: UserData[] = [
  { position: 1, expanseincomename: 'session abc', creditordebit: "Debit",date:'12/04/2021','id':1,'description':'djbjkas sdfdf',amount:12300 },
  { position: 2, expanseincomename: 'session def', creditordebit: "Credit",date:'12/04/2021','id':2 ,'description':'djbjkas sdfdf',amount:12300},
  { position: 3, expanseincomename: 'session ghi', creditordebit: "Debit",date:'12/04/2021','id':3 ,'description':'djbjkas sdfdf',amount:12300},
  { position: 4, expanseincomename: 'session klm ', creditordebit: "Credit",date:'12/04/2021','id':4 ,'description':'djbjkas sdfdf',amount:12300},
  { position: 5, expanseincomename: 'session abc', creditordebit: "Debit", date:'12/04/2021','id':5,'description':'djbjkas sdfdf',amount:12300 },
  { position: 6, expanseincomename: 'Board meeting', creditordebit: "Debit", date:'12/04/2021','id':6 ,'description':'djbjkas sdfdf',amount:12300},
  { position: 7, expanseincomename: 'Alumni meet', creditordebit: "Debit",date:'12/04/2021','id':7 ,'description':'djbjkas sdfdf',amount:12300},
  { position: 8, expanseincomename: 'wel come ceremony', creditordebit: "Debit",date:'12/04/2021','id':8 ,'description':'djbjkas sdfdf',amount:12300},
];

@Component({
  selector: 'app-fund',
  templateUrl: './fund.component.html',
  styleUrls: ['./fund.component.scss']
})
export class FundComponent implements OnInit {
  displayedColumns: string[] = ['no','expanseincomename','amount', 'date','action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  constructor(private router: Router) { }

  ngOnInit() {
  }
  addfund(){
    this.router.navigate(['/expense/add']);
  }
  deletefund(id){
    alert(id)
  }
  editfund(id)
  {
    this.router.navigate(['/expense/edit',id]);
  }
  viewfund(id){
    this.router.navigate(['/expense/view',id]);

  }

}
