import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FundComponent } from './fund.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FundRoutingModule } from './fund-routing.module';
import { MaterialModule } from 'src/app/shared/modules/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AddFundComponent } from './addfund.component';
import { ViewFundComponent } from './viewfund.component';

@NgModule({
  declarations: [FundComponent,AddFundComponent,ViewFundComponent],
  imports: [
    CommonModule,
    FundRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule.withConfig({ addFlexToParent: false })
  ]
})
export class FundModule { }
