import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';

export interface UserData {
  id:number;
  galleryeventname: string;
  imageurl: string;
  position:number;
  
}
const ELEMENT_DATA: UserData[] = [
  { position: 1, galleryeventname: 'session abc', imageurl:'virtual','id':1},
  { position: 2, galleryeventname: 'session def', imageurl:'physical','id':2 },
  { position: 3, galleryeventname: 'session ghi', imageurl:'virtual','id':3 },
  { position: 4, galleryeventname: 'session klm ', imageurl:'physical','id':4 },
  { position: 5, galleryeventname: 'session abc',  imageurl:'virtual','id':5},
  { position: 6, galleryeventname: 'Board meeting',  imageurl:'physical','id':6 },
  { position: 7, galleryeventname: 'Alumni meet', imageurl:'virtual','id':7},
  { position: 8, galleryeventname: 'wel come ceremony', imageurl:'physical','id':8},
];

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  displayedColumns: string[] = ['no','name', 'imageurl','action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  constructor(private router: Router) { }

  ngOnInit() {
  }
  addgallery(){
    this.router.navigate(['/gallery/add']);
  }
  deletegallery(id){
    alert(id)
  }
  editgallery(id)
  {
    this.router.navigate(['/gallery/edit',id]);
  }
  viewgallery(id){
    this.router.navigate(['/gallery/view',id]);

  }

}
