import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GalleryComponent } from './gallery.component';
import { AddGalleryComponent } from './addgallery.component';
import { ViewGalleryComponent } from './viewgallery.component';
import { EditGalleryComponent } from './editgallery.component';

const routes: Routes = [
    {
        path: '', component: GalleryComponent,
    },
    { path: 'add', component: AddGalleryComponent },
    { path: 'edit/:id', component: EditGalleryComponent },
    { path: 'view/:id', component: ViewGalleryComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GalleryRoutingModule {}