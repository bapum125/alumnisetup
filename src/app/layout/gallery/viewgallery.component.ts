import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-viewgallery',
  templateUrl: './viewgallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class ViewGalleryComponent implements OnInit {
  galleryId: any;
  updateJson:any;

  constructor(private router: ActivatedRoute) { }
  
  ngOnInit() {
    this.router.params.subscribe(params => {
      this.galleryId = params['id']; 
      this.updateJson = 
      {
        "galleryname":"aba",
       imageUrl : [
          '/assets/images/card-1.jpg',
          '/assets/images/card-1.jpg',
          '/assets/images/card-1.jpg',
          '/assets/images/card-1.jpg',
          '/assets/images/card-1.jpg',
          '/assets/images/card-1.jpg',
          '/assets/images/card-1.jpg'
        ]
      }

    })
  }
}
