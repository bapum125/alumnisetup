import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GalleryComponent } from './gallery.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { GalleryRoutingModule } from './gallery-routing.module';
import { MaterialModule } from 'src/app/shared/modules/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AddGalleryComponent } from './addgallery.component';
import { ViewGalleryComponent } from './viewgallery.component';
import { EditGalleryComponent } from './editgallery.component';

@NgModule({
  declarations: [GalleryComponent,AddGalleryComponent,ViewGalleryComponent,EditGalleryComponent],
  imports: [
    CommonModule,
    GalleryRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule.withConfig({ addFlexToParent: false })
  ]
})
export class GalleryModule { }
