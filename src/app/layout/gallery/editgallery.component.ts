import { Component, OnInit,ViewChild ,ElementRef} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-editgallery',
  templateUrl: './editgallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class EditGalleryComponent implements OnInit {
  myfilename: string;
  galleryname:any;
  galleryId: any;
  updateJson: any;
  ImagePath:any
  constructor(private router: ActivatedRoute) { }
  galleryForm: FormGroup;

  @ViewChild('UploadFileInput') uploadFileInput: ElementRef;
  
  ngOnInit() {
    this.router.params.subscribe(params => {
      this.galleryId = params['id']; 
  this.ImagePath = '/assets/images/card-1.jpg'

      this.updateJson = 
      {
        "galleryname":"aba",
       imageUrl : [
          '/assets/images/card-1.jpg',
          '/assets/images/card-1.jpg',
          '/assets/images/card-1.jpg',
          '/assets/images/card-1.jpg'
        ]
      }

    })
    this.galleryForm = new FormGroup({
      galleryname:new FormControl("",[Validators.required]),
    });
   
}
fileChangeEvent(fileInput: any) {

  if (fileInput.target.files && fileInput.target.files[0]) {


    this.myfilename = '';
    Array.from(fileInput.target.files).forEach((file: File) => {
      console.log(file);
      this.myfilename += file.name + ',';
    });

    const reader = new FileReader();
    reader.onload = (e: any) => {
      const image = new Image();
      image.src = e.target.result;
      image.onload = rs => {

        // Return Base64 Data URL
        const imgBase64Path = e.target.result;

      };
    };
    reader.readAsDataURL(fileInput.target.files[0]);

    // Reset File Input to Selct Same file again
    this.uploadFileInput.nativeElement.value = "";
  } 
}
onGalleryPost(){
  
}
removeImage(index){
  alert(index)
}

}
