import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})


export class ProfileComponent implements OnInit {


  constructor(private _formBuilder: FormBuilder) { }
  profileForm: FormGroup;
  usertype:string;


  ngOnInit() {
    this.usertype = localStorage.getItem('userType');
    this.profileForm = this._formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      phonenumber: ['', Validators.required],
      yearofpassing:['',Validators.required],
      presentAddress:['',Validators.required],
      permanentAddress:['',Validators.required],
      jobtype: ['', Validators.required],
      companysectorname: ['', Validators.required],
      designation: ['', Validators.required],
      maritalstatus: ['', Validators.required],
      bloodgroup: ['', Validators.required],
      gender:['', Validators.required],
      jobcountry: ['', Validators.required],
      jobcity: ['', Validators.required],
      skills:['',Validators.required]
    });
    this.profileForm.controls["name"].disable();
    this.profileForm.controls["email"].disable();
    this.profileForm.controls["yearofpassing"].disable();
    this.profileForm.controls["phonenumber"].disable();
  }
  updateProfile(){
    console.log(this.profileForm);
  }

}
