import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
            },
            {
                path: 'profile',
                loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)
            },
            {
                path: 'classment',
                loadChildren: () => import('./classment/classment.module').then(m => m.ClassmentModule)
            },
            {
                path: 'alumnis',
                loadChildren: () => import('./classment/classment.module').then(m => m.ClassmentModule)
            },
            {
                path: 'students',
                loadChildren: () => import('./classment/classment.module').then(m => m.ClassmentModule)
            },
            {
                path: 'job',
                loadChildren: () => import('./job/job.module').then(m => m.JobModule)
            },
            {
                path: 'settings',
                loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule)
            }
            ,
            {
                path: 'help',
                loadChildren: () => import('./help-request/help-request.module').then(m => m.HelpModule)
            },
            {
                path: 'openingjob',
                loadChildren: () => import('./openingjob/opening-job.module').then(m => m.OpeningJobModule)
            },
            {
                path: 'upload',
                loadChildren: () => import('./upload/upload.module').then(m => m.UploadModule)
            },
            {
                path: 'notifications',
                loadChildren: () => import('./notifications/notifications.module').then(m => m.NotificationModule)
            }
            ,
            {
                path: 'events',
                loadChildren: () => import('./event/event.module').then(m => m.EventModule)
            },
            {
                path: 'gallery',
                loadChildren: () => import('./gallery/gallery.module').then(m => m.GalleryModule)
            },
            {
                path: 'expense',
                loadChildren: () => import('./fund/fund.module').then(m => m.FundModule)
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule {}
