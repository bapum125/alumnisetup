import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JobComponent } from './job.component';
import { AddJobComponent } from './addjob.component';
import { ViewJobComponent } from './viewjob.component';

const routes: Routes = [
    {
        path: '', component: JobComponent,
    },
    { path: 'add', component: AddJobComponent },
    { path: 'edit/:id', component: AddJobComponent },
    { path: 'view/:id', component: ViewJobComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class JobRoutingModule {}
