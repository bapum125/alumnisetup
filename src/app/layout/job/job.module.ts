import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobComponent } from './job.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { JobRoutingModule } from './job-routing.module';
import { MaterialModule } from 'src/app/shared/modules/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AddJobComponent } from './addjob.component';
import { ViewJobComponent } from './viewjob.component';

@NgModule({
  declarations: [JobComponent,AddJobComponent,ViewJobComponent],
  imports: [
    CommonModule,
    JobRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule.withConfig({ addFlexToParent: false })
  ]
})
export class JobModule { }
