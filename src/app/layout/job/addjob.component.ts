import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-addjob',
  templateUrl: './addjob.component.html',
  styleUrls: ['./job.component.scss']
})
export class AddJobComponent implements OnInit {

  constructor(private router: ActivatedRoute) { }
  jobPostForm: FormGroup;
  buttonName:any;
  formName:any;
  jobId:any;
  updateJson:any;
  ngOnInit() {
    this.buttonName = "Post Job"
    this.formName = "Add"
    this.router.params.subscribe(params => {
      this.jobId = params['id']; 
      console.log(this.jobId)
      if(this.jobId !=undefined )
      {
        this.buttonName = "Update Job"
        this.formName = "Update"
        this.updateJson = {
          jobHeader:"aba",
          companyName:"TCS",
          experience:2,
          jobDescription:"sfsdfsdfsdfsdf fsdfsdf",
          skills:"java,dotnet,angular",
          email:"fasdasd@gmail.com"
        }
        console.log(this.updateJson)
      }else{
        this.updateJson = {
          jobHeader:"",
          companyName:"",
          experience:null,
          jobDescription:"",
          skills:"",
          email:""
        }
      }
   });
    this.jobPostForm = new FormGroup({
      jobheader:new FormControl("",[Validators.required]),
      companyName: new FormControl('',[Validators.required]),
      experience: new FormControl('',[Validators.required]),
      skills:new FormControl('',[Validators.required]),
      email: new FormControl('',Validators.email),
      jd:new FormControl('',[Validators.required])
    });
  }
  onJobPost()
  {
    console.log(this.jobPostForm)
  }
}
