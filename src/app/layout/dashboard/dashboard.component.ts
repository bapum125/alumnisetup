import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

export interface PeriodicElement {
    name: string;
    position: number;
    phonenumber: number;
    email: string;
    id:number
}

const ELEMENT_DATA: PeriodicElement[] = [
    { position: 1, name: 'Abhishekh Panda', phonenumber: 9625831254, email: 'abcd@gmail.com','id':1 },
    { position: 2, name: 'Gyana Nayak', phonenumber: 7854236589, email: 'abcde@gmail.com','id':2 },
    { position: 3, name: 'Duke', phonenumber: 7845256985, email: 'abcdef@gmail.com','id':3 },
    { position: 4, name: 'Nihar ', phonenumber: 8457695215, email: 'abcdefg@gmail.com','id':4 },
    { position: 5, name: 'Ajit', phonenumber: 7458962103, email: 'abcdefgh@gmail.com','id':5 },
];

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    displayedColumns = ['position', 'name', 'phonenumber', 'email','action'];
    dataSource = new MatTableDataSource(ELEMENT_DATA);
    userType:any;
    isProfileUpdate:boolean = true;
    myType:any;
    myData :any;
    options:any;
    width:any;
    height:any;

    constructor(private router: Router) {
    }

    ngOnInit() {
        this.myType = 'PieChart';
        this.myData =  [
            ['London', 8136000],
            ['New York', 8538000],
            ['Paris', 2244000],
            ['Berlin', 3470000],
            ['Kairo', 19500000]
          ];
          this.options = {  
            'is3D':   false,
          tooltip: {
              text: 'value'
          }  ,legend: 'none',
          pieSliceText: 'label',
          title: 'Country Wise Alumni Count',
          pieStartAngle: 100,
          };
          this.width = 550;
          this.height = 550;

          
        this.userType = localStorage.getItem('userType');
        // and check here also is previosly update or not
        if(this.userType == 'alumni' && !this.isProfileUpdate){
            alert('Please update your profile first it is most necessary');
        }
    }
    viewDetails(id)
    {
        this.router.navigate(['/classment/Details/'+id]);
    }
}
