import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    public showMenu: string;
    sideBarObj = {
      "alumniSideData":  [
        {
            "sideName":"Dashboard",
            "sideUrl":"/dashboard",
            "sideIcon":'dashboard'
        },
        {
            "sideName":"Profile",
            "sideUrl":"/profile",
            "sideIcon":'account_circle'
        },
        {
            "sideName":"Classment",
            "sideUrl":"/classment",
            "sideIcon":'home'
        },
        {   
            "sideName":"PostJob",
            "sideUrl":"/job",
            "sideIcon":'work_off'
        },
        {
            "sideName":"Settings",
            "sideUrl":"/settings",
            "sideIcon":'settings'
        }],
      "adminSideData":  [{
            "sideName":"Dashboard",
            "sideUrl":"/dashboard",
            "sideIcon":'dashboard'
        },
        {
            "sideName":"Alumnis",
            "sideUrl":"/alumnis",
            "sideIcon":'people'
        },
        {
            "sideName":"Students",
            "sideUrl":"/students",
            "sideIcon":'emoji_people'
        },
        {
            "sideName":"Events",
            "sideUrl":"/events",
            "sideIcon":'event'
        },
        {
            "sideName":"Gallery",
            "sideUrl":"/gallery",
            "sideIcon":'photo_camera'
        },
        {   
            "sideName":"Jobs",
            "sideUrl":"/job",
            "sideIcon":'work'
        },
        {
            "sideName":"Fund",
            "sideUrl":"/expense",
            "sideIcon":'account_balance_wallet'
        },
        {
            "sideName":"Settings",
            "sideUrl":"/settings",
            "sideIcon":'settings'
        }],
      "studentSideData":  [{
            "sideName":"Dashboard",
            "sideUrl":"/dashboard",
            "sideIcon":'dashboard'
        },
        {
            "sideName":"Profile",
            "sideUrl":"/profile",
            "sideIcon":'account_circle'
        },
        {
            "sideName":"Classment",
            "sideUrl":"/classment",
            "sideIcon":'home'
        },
        {
            "sideName":"Settings",
            "sideUrl":"/settings",
            "sideIcon":'settings'
        }],
      "collegeSideData":
        [{
            "sideName":"Dashboard",
            "sideUrl":"/dashboard",
            "sideIcon":'dashboard'
        },
        {
            "sideName":"settings",
            "sideUrl":"/settings",
            "sideIcon":'settings'
        }]
    }
    sideMenuData:any;
    userTypeLogin:any;
    constructor() {}

    ngOnInit() {
        this.showMenu = '';
        this.userTypeLogin = localStorage.getItem('userType');
        if(this.userTypeLogin === 'student')
        {
         this.sideMenuData = this.sideBarObj.studentSideData;
        }else if(this.userTypeLogin === 'admin')
        {
         this.sideMenuData = this.sideBarObj.adminSideData;
        }else if(this.userTypeLogin === 'alumni')
        {
         this.sideMenuData = this.sideBarObj.alumniSideData;
        };if(this.userTypeLogin === 'college')
        {
         this.sideMenuData = this.sideBarObj.collegeSideData;
        };
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }
}
