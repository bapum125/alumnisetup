
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { StatModule } from '../../shared/modules/stat/stat.module';

import { MaterialModule } from 'src/app/shared/modules/material/material.module';
import { HelpComponent } from './help-request.component';
import { HelpRoutingModule } from './help-request-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AddHelpComponent } from './addrequest.component';
import { ViewHelpComponent } from './viewrequest.component';

@NgModule({
    imports: [
        CommonModule,
        HelpRoutingModule,
        StatModule,
        MaterialModule,
        ReactiveFormsModule,
        FlexLayoutModule.withConfig({ addFlexToParent: false })
    ],
    declarations: [HelpComponent,AddHelpComponent,ViewHelpComponent]
})
export class HelpModule { }
