import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HelpComponent } from './help-request.component';
import { AddHelpComponent } from './addrequest.component';
import { ViewHelpComponent } from './viewrequest.component';

const routes: Routes = [
    {
        path: '',
        component: HelpComponent
    },
    {
        path: 'add',
        component: AddHelpComponent
    },
    {
        path: 'edit/:id',
        component: AddHelpComponent
    },
    {
        path: 'view/:id',
        component: ViewHelpComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HelpRoutingModule {}
