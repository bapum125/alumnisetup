import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';;


@Component({
  selector: 'app-add-request',
  templateUrl: './addrequest.component.html',
  styleUrls: ['./help-request.component.scss']
})
export class AddHelpComponent implements OnInit {
  constructor(private router: ActivatedRoute) { }
  requestForm: FormGroup;
  buttonName:any;
  formName:any;
  reqId:any;
  updateJson:any;
  ngOnInit() {
    this.buttonName = "Add"
    this.formName = "Add"
    this.router.params.subscribe(params => {
      this.reqId = params['id']; 
      console.log(this.reqId)
      if(this.reqId !=undefined )
      {
        this.buttonName = "Update"
        this.formName = "Update"
        this.updateJson = {
          requestDetails:"aba",
          querytype:"request"
        }
        console.log(this.updateJson)
      }else{
        this.updateJson = {
          requestDetails:"",
          querytype:""
        }
      }
   });
    this.requestForm = new FormGroup({
      requestdetails:new FormControl("",[Validators.required]),
      querytype:new FormControl("",[Validators.required]),
    });
  }
  onRequest()
  {
    console.log(this.requestForm)
  }

}

