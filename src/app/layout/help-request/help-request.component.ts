import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';

export interface UserData {
  id:number;
  position:number;
  requestID: string;
  requestDetails: string;
  IsApproved:boolean
}
const ELEMENT_DATA: UserData[] = [
  { 'position': 1, 'requestID': 'REQ9087908', 'requestDetails':'need blood','id':1,'IsApproved':true },
  { position: 2, requestID: 'REQ9087900','requestDetails':'need information', 'id':2 ,'IsApproved':null},
  { position: 3, requestID: 'REQ9087909', 'requestDetails':'need location','id':3 ,'IsApproved':false},
];

@Component({
  selector: 'app-help-request',
  templateUrl: './help-request.component.html',
  styleUrls: ['./help-request.component.scss']
})
export class HelpComponent implements OnInit {
  displayedColumns: string[] = ['no','requestid','requestDetails','status','action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  constructor(private router: Router) { }

  ngOnInit() {
  }
  addRequest(){
    this.router.navigate(['/help/add']);
  }
  deleteRequest(id){
    alert(id)
  }
  editRequest(id)
  {
    this.router.navigate(['/help/edit',id]);
  }
  viewRequest(id){
    this.router.navigate(['/help/view',id]);

  }

}

