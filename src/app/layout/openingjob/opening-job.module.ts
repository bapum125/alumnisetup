import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from 'src/app/shared/modules/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { OpeningJobComponent } from './opening-job.component';
import { ViewOpeningJobComponent } from './view-opening-job.component';
import { OpeningJobRoutingModule } from './opening-job-routing.module';

@NgModule({
  declarations: [OpeningJobComponent,ViewOpeningJobComponent],
  imports: [
    CommonModule,
    OpeningJobRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule.withConfig({ addFlexToParent: false })
  ]
})
export class OpeningJobModule { }
