import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OpeningJobComponent } from './opening-job.component';
import { ViewOpeningJobComponent } from './view-opening-job.component';

const routes: Routes = [
    {
        path: '', component: OpeningJobComponent,
    },
    { path: 'view/:id', component: ViewOpeningJobComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OpeningJobRoutingModule {}
