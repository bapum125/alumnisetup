import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';

export interface UserData {
  id:number;
  eventname: string;
  datetime: string;
  type: string;
  position:number;
  IsApproved:boolean;
  
}
const ELEMENT_DATA: UserData[] = [
  { position: 1, eventname: 'session abc', datetime: "12/04/2121,04:30pm",type:'virtual','id':1,'IsApproved':true },
  { position: 2, eventname: 'session def', datetime: "12/04/2121,05:30pm",type:'physical','id':2 ,'IsApproved':null},
  { position: 3, eventname: 'session ghi', datetime: "12/04/2121,06:30pm",type:'virtual','id':3 ,'IsApproved':false},
  { position: 4, eventname: 'session klm ', datetime: "12/04/2121,07:30pm",type:'physical','id':4 ,'IsApproved':true},
  { position: 5, eventname: 'session abc', datetime: "12/04/2121,09:30pm", type:'virtual','id':5,'IsApproved':false },
  { position: 6, eventname: 'Board meeting', datetime: "12/04/2121,01:30pm", type:'physical','id':6 ,'IsApproved':null},
  { position: 7, eventname: 'Alumni meet', datetime: "12/04/2121,02:30pm",type:'virtual','id':7 ,'IsApproved':true},
  { position: 8, eventname: 'wel come ceremony', datetime: "12/04/2121,11:30pm",type:'physical','id':8 ,'IsApproved':true},
];

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {
  displayedColumns: string[] = ['no','name','date', 'type','action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  constructor(private router: Router) { }

  ngOnInit() {
  }
  addevent(){
    this.router.navigate(['/events/add']);
  }
  deleteevent(id){
    alert(id)
  }
  editevent(id)
  {
    this.router.navigate(['/events/edit',id]);
  }
  viewevent(id){
    this.router.navigate(['/events/view',id]);

  }

}
