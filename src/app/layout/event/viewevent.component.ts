import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-viewevent',
  templateUrl: './viewevent.component.html',
  styleUrls: ['./event.component.scss']
})
export class ViewEventComponent implements OnInit {

  constructor(private router: ActivatedRoute) { }
  eventPostForm: FormGroup;
  formName:any;
  eventId:any;
  updateJson:any;
  ngOnInit() {
    this.formName = "View"
    this.router.params.subscribe(params => {
      this.eventId = params['id']; 
      console.log(this.eventId)
      if(this.eventId !=undefined )
      {
        this.updateJson = {
          eventName:"aba",
          eventDescription:"TCS",
          eventDateTime:"12/10/2021",
          type:"virtual",
          location:"java,dotnet,angular",
          url:"fasdasd@gmail.com",
          eventCredentials:"fasdasd@gmail.com",
          eventPhoto:'/assets/images/card-1.jpg'
        }
        console.log(this.updateJson)
      }
   });
  }
}
