import { Component, OnInit,ViewChild ,ElementRef} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-addevent',
  templateUrl: './addevent.component.html',
  styleUrls: ['./event.component.scss']
})
export class AddEventComponent implements OnInit {
  imageUrl: string | ArrayBuffer;
  @ViewChild('UploadFileInput') uploadFileInput: ElementRef;
  eventType=[{eventTypeName:'physical'},{eventTypeName:'virtual'}]
  ImagePath: string;

  constructor(private router: ActivatedRoute) { }
  EventForm: FormGroup;
  buttonName:any;
  formName:any;
  eventId:any;
  updateJson:any;
  isPhysical:boolean = false;
  isVirtual:boolean = false;
  myfilename = 'Select File';
  ngOnInit() {
    this.ImagePath = '/assets/images/card-1.jpg'
    this.buttonName = "Add event"
    this.formName = "Add"
    this.router.params.subscribe(params => {
      this.eventId = params['id']; 
      console.log(this.eventId)
      if(this.eventId !=undefined )
      {
        this.buttonName = "Update event"
        this.formName = "Update"
        this.updateJson = {
          eventname:"aba",
          eventDescription:"TCS",
          eventType:"virtual",
          location:"sfsdfsdfsdfsdf fsdfsdf",
          eventUrl:"java,dotnet,angular",
          eventcredentilas:"fasdasd@gmail.com",
          eventdate:"2021-04-04"
        }
        // this.EventForm.patchValue({eventType:this.updateJson.eventType})
        if(this.updateJson.eventType == 'virtual')
        {
          this.isVirtual = true;
        }
        console.log(this.updateJson)
      }else{
        this.updateJson = {
          eventname:"",
          eventDescription:"",
          eventType:null,
          location:"",
          eventUrl:"",
          eventcredentilas:"",
          eventdate:""

        }
      }
   });
    this.EventForm = new FormGroup({
      eventname:new FormControl("",[Validators.required]),
      description: new FormControl('',[Validators.required]),
      eventType: new FormControl('',[Validators.required]),
      location:new FormControl('',[]),
      eventUrl: new FormControl('',[]),
      eventdate: new FormControl('',[Validators.required]),
      imageData: new FormControl('',[]),
      eventcredentilas:new FormControl('',[])
    });
}
onEventPost()
{
  console.log(this.EventForm)
}
onSelect(event){
  console.log(event)
  if(event == 'physical')
  {
    this.isPhysical = true;
    this.isVirtual = false;
    this.updateJson.eventcredentilas = " ";
    this.EventForm.patchValue({eventcredentilas: ''});
    this.updateJson.eventUrl = '';
  }
  if(event == 'virtual')
  {
    this.isPhysical = false;
    this.updateJson.location = '';
    this.isVirtual = true;
  }
}
fileChangeEvent(fileInput: any) {

  if (fileInput.target.files && fileInput.target.files[0]) {


    this.myfilename = '';
    Array.from(fileInput.target.files).forEach((file: File) => {
      console.log(file);
      this.myfilename += file.name + ',';
    });

    const reader = new FileReader();
    reader.onload = (e: any) => {
      const image = new Image();
      image.src = e.target.result;
      image.onload = rs => {

        // Return Base64 Data URL
        const imgBase64Path = e.target.result;

      };
    };
    reader.readAsDataURL(fileInput.target.files[0]);

    // Reset File Input to Selct Same file again
    this.uploadFileInput.nativeElement.value = "";
  } else {
    this.myfilename = 'Select File';
  }
}
}
