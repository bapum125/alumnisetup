import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventComponent } from './event.component';
import { AddEventComponent } from './addevent.component';
import { ViewEventComponent } from './viewevent.component';

const routes: Routes = [
    {
        path: '', component: EventComponent,
    },
    { path: 'add', component: AddEventComponent },
    { path: 'edit/:id', component: AddEventComponent },
    { path: 'view/:id', component: ViewEventComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EventRoutingModule {}
