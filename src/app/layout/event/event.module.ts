import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventComponent } from './event.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { EventRoutingModule } from './event-routing.module';
import { MaterialModule } from 'src/app/shared/modules/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AddEventComponent } from './addevent.component';
import { ViewEventComponent } from './viewevent.component';

@NgModule({
  declarations: [EventComponent,AddEventComponent,ViewEventComponent],
  imports: [
    CommonModule,
    EventRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule.withConfig({ addFlexToParent: false })
  ]
})
export class EventModule { }
