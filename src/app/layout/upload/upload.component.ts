import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  constructor(private _formBuilder: FormBuilder) { }
  @ViewChild('fileInput') el: ElementRef;
  imageUrl:any;
  userType:any;
  isAdminOrCollege:boolean = false;
  ngOnInit() {
    this.userType = localStorage.getItem('userType');
    if(this.userType === 'admin' || this.userType === 'college')
    {
      this.isAdminOrCollege = true;
    }
  }
  onImageSubmit(){
  }
  uploadFile(event) {
    console.log(event)
    let reader = new FileReader(); // HTML5 FileReader API
    let file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);

      // When file uploads set it to file formcontrol
      reader.onload = () => {
        this.imageUrl = reader.result;
      }
    }
  }
  uploadDoc(event){
    console.log(event)
    let reader = new FileReader(); // HTML5 FileReader API
    let file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);

      // When file uploads set it to file formcontrol
      reader.onload = () => {
        this.imageUrl = reader.result;
      }
    }
  }

}
