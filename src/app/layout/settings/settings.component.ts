import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  constructor(private router: Router) { }


  ngOnInit() {
    
  }
  redirecttopage(data){
    if(data === 'help')
    {
      this.router.navigate(['/help']);
    }else if(data === 'job')
    {
      this.router.navigate(['/openingjob']);
    }else if(data === 'image')
    {
      this.router.navigate(['/upload']);

    }else if(data === 'notification'){
      this.router.navigate(['/notifications']);

    }
  }


}
