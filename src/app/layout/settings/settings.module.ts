
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { StatModule } from '../../shared/modules/stat/stat.module';

import { MaterialModule } from 'src/app/shared/modules/material/material.module';
import { SettingsComponent } from './settings.component';
import { SettingsRoutingModule } from './settings-routing.module';

@NgModule({
    imports: [
        CommonModule,
        SettingsRoutingModule,
        StatModule,
        MaterialModule,
        FlexLayoutModule.withConfig({ addFlexToParent: false })
    ],
    declarations: [SettingsComponent]
})
export class SettingsModule { }
