import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Router,NavigationEnd } from '@angular/router';

export interface UserData {
  id:number;
  name: string;
  email: string;
  phonenumber: number;
  position:number;
  yearofpassing:number;
  status:string;
  
}
const ELEMENT_DATA: UserData[] = [
  { position: 1, name: 'Abhishekh Panda', phonenumber: 9625831254, email: 'abcd@gmail.com','id':1,yearofpassing:2008,status:'pending' },
  { position: 2, name: 'Gyana Nayak', phonenumber: 7854236589, email: 'abcde@gmail.com','id':2,yearofpassing:2008,status:'reject' },
  { position: 3, name: 'Duke', phonenumber: 7845256985, email: 'abcdef@gmail.com','id':3,yearofpassing:2008,status:'approved' },
  { position: 4, name: 'Nihar ', phonenumber: 8457695215, email: 'abcdefg@gmail.com','id':4 ,yearofpassing:2008,status:'pending'},
  { position: 5, name: 'Ajit', phonenumber: 7458962103, email: 'abcdefgh@gmail.com','id':5,yearofpassing:2008,status:'approved' },
  { position: 6, name: 'Badal', phonenumber: 7458782103, email: 'abcdefgh@gmail.com','id':6 ,yearofpassing:2008,status:'pending'},
  { position: 7, name: 'Sankar', phonenumber: 74889962103, email: 'abcdefgh@gmail.com','id':7 ,yearofpassing:2008,status:'approved'},
  { position: 8, name: 'Dinesh', phonenumber: 74556262103, email: 'abcdefgh@gmail.com','id':8 ,yearofpassing:2008,status:'pending'},
];

@Component({
  selector: 'app-classment',
  templateUrl: './classment.component.html',
  styleUrls: ['./classment.component.scss']
})
export class ClassmentComponent implements OnInit {

  displayedColumns: string[];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  userType:any;
  formHeader:any;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  constructor(private router: Router) {
   }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');
    console.log(this.userType);
    if(this.router.url === '/alumnis'){
      
      this.formHeader = "Alumnis List";
      localStorage.setItem("backurl",this.router.url);
      this.displayedColumns = ['no','name', 'email', 'phonenumber','batch','status','View'];
    }else if(this.router.url === '/students'){
      this.formHeader = "Students List";
      localStorage.setItem("backurl",this.router.url);
      this.displayedColumns = ['no','name', 'email', 'phonenumber','batch','status','View'];
    }else{
      this.formHeader = 'Classment List';
      localStorage.setItem("backurl",this.router.url);
      this.displayedColumns = ['no','name', 'email', 'phonenumber','View'];
    }

  }
  redirectToDetails(id){
    console.log(id);
    this.router.navigate([this.router.url+'/Details',id]);
  }

}
