import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-viewclsment',
  templateUrl: './viewclsmentdetails.component.html',
  styleUrls: ['./classment.component.scss']
})
export class ViewClsmentDetailsComponent implements OnInit {

  constructor() { }
  backUrl:string;
  isStudent:boolean = false;

  ngOnInit() {
    this.backUrl = localStorage.getItem('backurl');
    if(this.backUrl === '/students')
    {
      this.isStudent = true;
    }else{
      this.isStudent = false;
    }
    console.log(this.backUrl)
  }
}
