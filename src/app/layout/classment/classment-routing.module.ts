import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClassmentComponent } from './classment.component';
import { ViewClsmentDetailsComponent } from './viewclsmentdetails.component';

const routes: Routes = [
    {
        path: '', component: ClassmentComponent,
    },
    { path: 'Details/:id', component: ViewClsmentDetailsComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ClaasmentRoutingModule {}
