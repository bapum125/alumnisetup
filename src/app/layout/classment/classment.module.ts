import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassmentComponent } from './classment.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ClaasmentRoutingModule } from './classment-routing.module';
import { MaterialModule } from 'src/app/shared/modules/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ViewClsmentDetailsComponent } from './viewclsmentdetails.component';

@NgModule({
  declarations: [ClassmentComponent,ViewClsmentDetailsComponent],
  imports: [
    CommonModule,
    ClaasmentRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule.withConfig({ addFlexToParent: false })
  ]
})
export class ClassmentModule { }
