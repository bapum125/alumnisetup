import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from 'src/app/shared/modules/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NotificationComponent } from './notifications.component';
import { NotificationRoutingModule } from './notifications-routing.module';
import { ViewNotificationComponent } from './viewnotifications.component';

@NgModule({
  declarations: [NotificationComponent,ViewNotificationComponent],
  imports: [
    CommonModule,
    NotificationRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule.withConfig({ addFlexToParent: false })
  ]
})
export class NotificationModule { }
