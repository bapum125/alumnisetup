import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';

export interface UserData {
  id:number;
  companyname: string;
  email: string;
  experience: number;
  position:number;
  IsApproved:boolean;
  
}
const ELEMENT_DATA: UserData[] = [
  { position: 1, companyname: 'cirruscloud system', experience: 9625831254, email: 'abcd@gmail.com','id':1,'IsApproved':true },
  { position: 2, companyname: 'Ajatus', experience: 7854236589, email: 'abcde@gmail.com','id':2 ,'IsApproved':null},
  { position: 3, companyname: 'Iserveu', experience: 7845256985, email: 'abcdef@gmail.com','id':3 ,'IsApproved':false},
  { position: 4, companyname: 'Virtus ', experience: 8457695215, email: 'abcdefg@gmail.com','id':4 ,'IsApproved':true},
  { position: 5, companyname: 'TCS', experience: 7458962103, email: 'abcdefgh@gmail.com','id':5,'IsApproved':false },
  { position: 6, companyname: 'Infosys', experience: 7458782103, email: 'abcdefgh@gmail.com','id':6 ,'IsApproved':null},
  { position: 7, companyname: 'IBM', experience: 74889962103, email: 'abcdefgh@gmail.com','id':7 ,'IsApproved':true},
  { position: 8, companyname: 'HCL', experience: 74556262103, email: 'abcdefgh@gmail.com','id':8 ,'IsApproved':true},
];

@Component({
  selector: 'app-notification',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationComponent implements OnInit {
  displayedColumns: string[] = ['no','companyname','experience', 'email','action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  constructor(private router: Router) { }

  ngOnInit() {
  }
  
  viewJob(id){
    this.router.navigate(['/notifications/view',id]);

  }

}
