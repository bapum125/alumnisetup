import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotificationComponent } from './notifications.component';
import { ViewNotificationComponent } from './viewnotifications.component';

const routes: Routes = [
    {
        path: '', component: NotificationComponent,
    },
    { path: 'view/:id', component: ViewNotificationComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NotificationRoutingModule {}
